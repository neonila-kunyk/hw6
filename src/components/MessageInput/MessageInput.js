import React from 'react';
import './MessageInput.css';

import PropTypes from 'prop-types';

class MessageInput extends React.Component {
  render() {
    const buttonСlassName = 'message-input-button' + (this.props.messageEditing ? ' blue' : ' violet');
    return (
        <div className='message-input'>
          <input
              className='message-input-text'
              type='text'
              placeholder='Type a message here and press Enter'
              onChange={this.props.onStateChange}
              value={this.props.input}
          />
          <button className={buttonСlassName} onClick={this.props.onSend}>
            {this.props.messageEditing ? <i className='fas fa-check-square'></i> : <i className='fa fa-paper-plane'></i>}
          </button>
        </div>
    )
  }
}

export default MessageInput;

MessageInput.propTypes = {
  messageEditing: PropTypes.number,
  onStateChange: PropTypes.func,
  input: PropTypes.string,
  onSend: PropTypes.func
};