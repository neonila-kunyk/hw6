import React from 'react';
import './Preloader.css';

class Preloader extends React.Component {
  render() {
    return (
      <div className='preloader'>
        <div className='pencil'>
          <div className='pencil__ball-point'></div>
          <div className='pencil__cap'></div>
          <div className='pencil__cap-base'></div>
          <div className='pencil__middle'></div>
          <div className='pencil__eraser'></div>
        </div>
        <div className='line'></div>
        <h2>Page loading...please wait</h2>
      </div>
    )
  }
}

export default Preloader;