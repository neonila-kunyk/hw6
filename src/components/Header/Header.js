import React from 'react';
import './Header.css';

import PropTypes from 'prop-types';

class Header extends React.Component {
  render() {
    return (
        <div className='header'>
          <i className="fas fa-pen-nib"></i>
          <span className='header-title'>{this.props.chatName}</span>
          <span className='header-users-count'>{this.props.usersCount}</span>
          <span className='header-messages-count'>{this.props.messagesCount}</span>
          <span className='header-last-message-date'>{this.props.lastMessageDate}</span>
        </div>
    )
  }
}

export default Header;

Header.propTypes = {
  chatName: PropTypes.string,
  usersCount: PropTypes.number,
  messagesCount: PropTypes.number,
  lastMessageDate: PropTypes.string
};