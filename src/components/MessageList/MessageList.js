import React from 'react';
import Message from '../Message/Message.js'
import OwnMessage from '../OwnMessage/OwnMessage.js'
import './MessageList.css';

import moment from 'moment';
import PropTypes from 'prop-types';

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.messageList = React.createRef();
  }

  scrollToBottom = () => {
    if(this.props.isNeedToScroll && this.props.messages.length > 0) {
      this.messageList.current.scrollIntoView({behavior: 'smooth', block: 'end', inline: 'nearest'});
    }
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  renderItem(message, index, likeMessage, dislikeMessage, deleteMessage, editMessage) {
    if (message.type === 'other') {
      return (
        <Message
          key={message.id}
          avatar={message.avatar}
          user={message.user}
          text={message.text}
          time={moment(new Date(message.createdAt)).format('HH:m')}
          isLiked={message.isLiked}
          isDisliked={message.isDisliked}
          index={index}
          onLikeMessage={likeMessage}
          onDislikeMessage={dislikeMessage}
        />
      )
    }
    else {
      return (
        <OwnMessage
          key={message.id}
          text={message.text}
          time={moment(new Date(message.createdAt)).format('HH:m')}
          index={index}
          onDeleteMessage={deleteMessage}
          onEditMessage={editMessage}
        />
      );
    }
  }

  renderWithDivider(date) {
    const today = moment();
    const yesterday = moment().subtract(1, 'day');
    return (
      <div className='messages-divider' key={date.valueOf()}>
        <div className='divider-line'></div>
        <span className='date-label'>{   
          moment(new Date(date)).isSame(today, 'day') ? 'Today' :
          moment(new Date(date)).isSame(yesterday, 'day') ? 'Yesterday' :
          moment(new Date(date)).format('dddd, DD MMMM')
        }</span>
      </div>)
  }

  render() {
    const {messages, onDeleteMessage, onEditMessage, onLikeMessage, onDislikeMessage} = this.props;
    if(messages.length > 0) {
      return (
          <div className='message-list'>
            <div className='message-list-items' ref={this.messageList}>
              {this.renderWithDivider(new Date(messages[0].createdAt))}
              {messages.map((message, index) => {
                if (index !== 0 && !moment(new Date(messages[index-1].createdAt)).isSame(new Date(message.createdAt), 'day')) {
                  return <React.Fragment key={index}>
                            {this.renderWithDivider(message.createdAt)}
                            {this.renderItem(message, index, onLikeMessage, onDislikeMessage, onDeleteMessage, onEditMessage)}
                         </React.Fragment>;
                } else return this.renderItem(message, index, onLikeMessage, onDislikeMessage, onDeleteMessage, onEditMessage);
              })}
            </div>
          </div>
      )
    } else {
      return null;
    }
  }
}

export default MessageList;

MessageList.propTypes = {
  messages: PropTypes.array,
  onDeleteMessage: PropTypes.func,
  onEditMessage: PropTypes.func,
  onLikeMessage: PropTypes.func,
  onDislikeMessage: PropTypes.func
};