import React from 'react';
import Preloader from '../Preloader/Preloader.js'
import Header from '../Header/Header.js'
import MessageList from '../MessageList/MessageList.js'
import MessageInput from '../MessageInput/MessageInput.js'
import './Chat.css';

import sendRequest from '../../helpers/sender-req.js'
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';

class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      isLoading: true,
      messages: [],
      input: '',
      messageEditing: 0,
      isNeedToScroll: true,
      currentUser: {
          userId: uuidv4(),
          avatar: 'https://okeygeek.ru/wp-content/uploads/2020/03/no_avatar.png',
          user: 'Alin'
      }
    }
  }

  componentDidMount() {
    sendRequest(this.props.url)
      .then(
        data => {
        data.map(message => {
          message.type = 'other';
          message.isLiked = false;
          message.isDisliked = false;
          return message;
        })
        data.sort((a,b) => new Date(a.createdAt) - new Date(b.createdAt));
        this.setState({ isLoading: false, messages: data});  
      })

    window.addEventListener('keydown', (e) => {
      if (e.code === 'Enter') {
        this.sendMessage();
      }
    });
  }

  sendMessage = () => {
    if (this.state.input !== '') {
      const updatedMessages = [...this.state.messages];
      const messageEditing = this.state.messageEditing;
      if (messageEditing) {
        updatedMessages[messageEditing].text = this.state.input;
        updatedMessages[messageEditing].editedAt = (new Date()).toISOString();
        this.setState({messageEditing: 0, isNeedToScroll: false});
      } else {
        updatedMessages.push({
          id: uuidv4(),
          userId: this.state.currentUser.userId,
          avatar: this.state.currentUser.avatar,
          user: this.state.currentUser.user,
          text: this.state.input,
          createdAt: (new Date()).toISOString(),
          editedAt: ''
        })
        this.setState({isNeedToScroll: true});
      }
      this.setState({messages: updatedMessages, input: ''});
    }
  } 

  deleteMessage = (index) => {
    const updatedMessages = [...this.state.messages]
    updatedMessages.splice(index, 1)
    this.setState({messages: updatedMessages, isNeedToScroll: false});
  }

  editMessage = (index) => {
    this.setState({messageEditing: index});
    this.setState({ input: this.state.messages[index].text, isNeedToScroll: false});
  }

  likeMessage = (index) => {
    const updatedMessages = [...this.state.messages];
    updatedMessages[index].isLiked = !updatedMessages[index].isLiked;
    updatedMessages[index].isDisliked = false;
    this.setState({messages: updatedMessages, isNeedToScroll: false});
  }

  dislikeMessage = (index) => {
    const updatedMessages = [...this.state.messages];
    updatedMessages[index].isLiked = false;
    updatedMessages[index].isDisliked = !updatedMessages[index].isDisliked;
    this.setState({messages: updatedMessages, isNeedToScroll: false});
  }

  onStateChange = (e) => {
    this.setState({ input: e.target.value });
  };

  getusersCount = () => {
    const usersIds = this.state.messages.map(message => message.userId);
    const usersSet = new Set(usersIds)
    return usersSet.size
  }

  getlastMessageDate = () => {
    const dates = this.state.messages.map(message => message.createdAt)
    const lastDate = dates.sort((a,b) => new Date(b) - new Date(a))[0]
    return moment(lastDate).format('DD.MM.YYYY HH:m');
  }

  render() {
    return <div className='chat'>
      {this.state.isLoading ? <Preloader /> : 
      <div className='wrapper'>
        <Header 
          chatName='Texting' 
          messagesCount={this.state.messages.length}
          usersCount={this.getusersCount()}
          lastMessageDate={this.getlastMessageDate()}
        />
        <MessageList 
          messages={this.state.messages}
          onDeleteMessage={this.deleteMessage}
          onEditMessage={this.editMessage}
          onLikeMessage={this.likeMessage}
          onDislikeMessage={this.dislikeMessage}
          isNeedToScroll={this.state.isNeedToScroll}
        />
        <MessageInput 
        onStateChange={this.onStateChange} 
        input={this.state.input} 
        onSend={this.sendMessage}
        messageEditing={this.state.messageEditing}
        />
      </div>}
    </div>
 }
}

export default Chat;

Chat.propTypes = {
  url: PropTypes.string
};