import React from 'react';
import './OwnMessage.css';

import PropTypes from 'prop-types';

class OwnMessage extends React.Component {
  render() {
    return (
        <div className='own-message'>
          <span className='message-text'>{this.props.text}</span>
          <span className='message-time'>{this.props.time}</span>
          <i className='fas fa-edit message-edit' onClick={() => this.props.onEditMessage(this.props.index)}></i>
          <i className='fas fa-trash message-delete' onClick={() => this.props.onDeleteMessage(this.props.index)}></i>
        </div>
    )
  }
}

export default OwnMessage;

OwnMessage.propTypes = {
  text: PropTypes.string,
  time: PropTypes.string,
  index: PropTypes.number
};